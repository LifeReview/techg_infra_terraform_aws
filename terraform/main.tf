provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_s3_bucket" "funniqbackups" {
  bucket = "funniqbackups"
  acl    = "private"
}

# ------------------------------------------------------- VPC ------------------------------------------------------------- #

resource "aws_vpc" "techg_vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "Review_App"
  }
}

# ------------------------------------------------------- Internet Gateway ------------------------------------------------------------- #

resource "aws_internet_gateway" "funniq_internet_gateway" {
  vpc_id = aws_vpc.techg_vpc.id

  tags = {
    Name = "Funniq IG"
  }
}

# ---------------------------------------------------- Subnets ------------------------------------------------------------- #

resource "aws_subnet" "ap_southeast_1b_private" {
  vpc_id     = aws_vpc.techg_vpc.id
  cidr_block = "10.0.2.0/24"
  
  tags = {
    Name = "10.0.2.0-ap-southeast-1b-private"
  }
}

resource "aws_subnet" "ap_southeast_1b_public" {
  vpc_id     = aws_vpc.techg_vpc.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  
  tags = {
    Name = "10.0.1.0-ap-southeast-1b-public"
  }
}

resource "aws_subnet" "ap_southeast_1c_public" {
  vpc_id     = aws_vpc.techg_vpc.id
  cidr_block = "10.0.3.0/24"
  map_public_ip_on_launch = "true"
  
  tags = {
    Name = "10.0.3.0-ap-southeast-1c-public"
  }
}

# ---------------------------------------------------- Security Group ------------------------------------------------------------- #

resource "aws_security_group" "review_app_public" {
  name        = "ReviewAppPublic"
  description = "allow 3000, ssh 22"
  vpc_id      = aws_vpc.techg_vpc.id

ingress = [
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = ""
    from_port        = 22
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 22
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = ""
    from_port        = 3000
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 3000
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = ""
    from_port        = 443
    ipv6_cidr_blocks = [
        "::/0",
    ]
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 443
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = ""
    from_port        = 5050
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 5050
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = ""
    from_port        = 5054
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 5054
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = ""
    from_port        = 5055
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 5055
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = ""
    from_port        = 8001
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 8001
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = ""
    from_port        = 8080
    ipv6_cidr_blocks = ["::/0",]
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 8080
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = ""
    from_port        = 80
    ipv6_cidr_blocks = ["::/0",]
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 80
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "Node exporter"
    from_port        = 3306
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 3306
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "Swarm cluster management communications"
    from_port        = 2377
    ipv6_cidr_blocks = ["::/0",]
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 2377
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "Docker swarm communication among nodes"
    from_port        = 7946
    ipv6_cidr_blocks = ["::/0",]
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 7946
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "Docker swarm communication among nodes"
    from_port        = 7946
    ipv6_cidr_blocks = ["::/0",]
    prefix_list_ids  = []
    protocol         = "udp"
    security_groups  = []
    self             = false
    to_port          = 7946
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "node exporter"
    from_port        = 9100
    ipv6_cidr_blocks = ["::/0",]
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 9100
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "docker swarm overlay network traffic"
    from_port        = 4789
    ipv6_cidr_blocks = ["::/0",]
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 4789
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "docker swarm overlay network traffic"
    from_port        = 4789
    ipv6_cidr_blocks = ["::/0",]
    prefix_list_ids  = []
    protocol         = "udp"
    security_groups  = []
    self             = false
    to_port          = 4789
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "rails"
    from_port        = 3001
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 3001
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "docker swarm portainer"
    from_port        = 9000
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 9000
  },
  {
    cidr_blocks      = ["0.0.0.0/0",]
    description      = "docker swarm portainer"
    from_port        = 8000
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 8000
  },
]

  egress = [
  {
    cidr_blocks      = ["0.0.0.0/0"]
    description      = ""
    from_port        = 0
    ipv6_cidr_blocks = ["::/0"]
    prefix_list_ids  = []
    protocol         = "-1"
    security_groups  = []
    self             = false
    to_port          = 0
  },
]

  tags = {
    Name = "Techg Funniq SG"
  }
}

# ---------------------------------------------------- Network Interfaces ------------------------------------------------------------- #

resource "aws_network_interface" "aws_funniq_build_ni" {
  subnet_id   = aws_subnet.ap_southeast_1b_public.id
  private_ips = ["10.0.1.16"]
  security_groups = [aws_security_group.review_app_public.id]

  # attachment {
  #   instance     = aws_instance.aws_funniq_build.id
  #   device_index = 0
  # }
  
  tags = {
    Name = "aws_funniq_build_ni"
  }
}

resource "aws_network_interface" "aws_funniq_deploy_ni" {
  subnet_id   = aws_subnet.ap_southeast_1b_public.id
  private_ips = ["10.0.1.118"]  
  security_groups = [aws_security_group.review_app_public.id]

  attachment {
    instance     = aws_instance.aws_funniq_deploy.id
    device_index = 0
  }

  tags = {
    Name = "aws_funniq_deploy_ni"
  }
}

# ---------------------------------------------------- EC2 Instance ------------------------------------------------------------- #

resource "aws_instance" "aws_funniq_build" {
  ami                     = "ami-01581ffba3821cdf3" # us-west-2
  instance_type           = "t2.micro"
  key_name                = "funniq_key_pair_21_4_21"
  tenancy                 = "default"
  # vpc_security_group_ids  = [aws_security_group.review_app_public.id,]  

  ebs_block_device {
    device_name           = "/dev/sda1"
    delete_on_termination = true
    encrypted             = false
    volume_size           = 10
    volume_type           = "gp2"
  }

  network_interface {
    network_interface_id = aws_network_interface.aws_funniq_build_ni.id
    device_index         = 0
  }

  credit_specification {
    cpu_credits = "standard"
  }

  lifecycle {
    create_before_destroy = false
  }

  tags = {
    Name = "aws_funniq_build"
  }
}

resource "aws_instance" "aws_funniq_deploy" {
  ami                           = "ami-05b891753d41ff88f"
  instance_type                 = "t2.medium"
  key_name                      = "funniq_key_pair_21_4_21"  

  credit_specification {
    cpu_credits = "standard"
  }

  tags = {
    Name = "aws_funniq_deploy"
  }
}

# ------------------------------------------------------------ EBS volume ------------------------------------------------------------- #

# resource "aws_ebs_volume" "funniq_build_ebs" {
#   availability_zone = "ap-southeast-1b"
#   size              = 20
#   type              = "gp2"

#   tags = {
#     Name = "funniq build ebs"
#   }
# }

# resource "aws_ebs_volume" "funniq_deploy_ebs" {
#   availability_zone = "ap-southeast-1b"
#   size              = 20
#   type              = "gp2"

#   tags = {
#     Name = "funniq deploy ebs"
#   }
# }

# -------------------------------------------------- EBS volume attachment------------------------------------------------------------- #

# resource "aws_volume_attachment" "ebs_att" {
#   device_name = "/dev/sda1"
#   volume_id   = aws_ebs_volume.funniq_build_ebs.id
#   instance_id = aws_instance.aws_funniq_build.id
# }

# ---------------------------------------------------- Elastic IP address ------------------------------------------------------------- #

resource "aws_eip" "funniq_build_eip" {
  vpc = true

  # instance                  = aws_instance.aws_funniq_build.id  
  depends_on                = [aws_internet_gateway.funniq_internet_gateway]

  tags = {
    Name = "funniq_build_eip"
  }
}

resource "aws_eip" "funniq_deploy_eip" {
  vpc = true

  instance                  = aws_instance.aws_funniq_deploy.id  
  depends_on                = [aws_internet_gateway.funniq_internet_gateway]

  tags = {
    Name = "funniq_deploy_eip"
  }
}